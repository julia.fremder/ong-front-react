import React from 'react'
import Header from '../layout/header'
import Footer from '../layout/footer'
import styled from 'styled-components';

const Layout = (props) => {
    return (
        <>
            <Header/>
            <Main className="container">
                {props.children}
            </Main>
            <Footer/>
        </>
    )
}

export default Layout


const Main = styled.main`

    flex:1;

`