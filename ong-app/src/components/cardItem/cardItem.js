import React from 'react';
import { Link } from 'react-router-dom';
import { Card, CardMedia, CardContent, CardActions, Button, Typography} from '@material-ui/core';
import useStyles from '../../assets/styles';

const CardItem = (props) => {

    const { id, description, image_URL } = props.item

    const classes = useStyles();
 
    return (
        <Card className={classes.card}>
            <CardMedia
              className={classes.cardMedia}
              image={image_URL}
              title="Image title"
            />
            <CardContent className={classes.cardContent}>
              <Typography gutterBottom variant="h6">
                {description}
              </Typography>
              <Typography>{description}</Typography>
            </CardContent>
            <CardActions>
              <Button size="small" tag={Link} href={`/actions/${id}`} color="primary">
                Entrar
              </Button>
            </CardActions>
          </Card>
    )
}

export default CardItem
