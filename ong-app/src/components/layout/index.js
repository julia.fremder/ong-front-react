import React from 'react';
import {  CssBaseline } from '@material-ui/core';
import Footer from './footer';
import Header from './header';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';



import useStyles from '../../assets/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      light:'#ff94c2',
      main: '#f06292',
      dark:'#ba2d65'
    },
    secondary: {
      
      light:'#ffeeff',
      main: '#f8bbd0',
      dark:'#c48b9f'
    },
  },
});

const App = (props) => {

  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <div className={classes.layout}>
        <CssBaseline/>
        <Header/>
        <main className={classes.mainChildren}>
          {props.children}
        </main>
        <Footer/>
      </div>
      </ThemeProvider>
  );

}

export default App;