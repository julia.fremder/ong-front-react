import React from 'react';
import { AppBar, Container, Toolbar, Typography } from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/People'; 
import useStyles from '../../assets/styles';

const Header = () => {

    const classes = useStyles();

    return (
        
        <AppBar position="relative">
            <Container>
          <Toolbar>
            <PeopleIcon className={classes.icon}/>
            <Typography variant="h6">
              Voluntários de Cascais
            </Typography>
          </Toolbar>
          </Container>
        </AppBar>
       
    )
}

export default Header
