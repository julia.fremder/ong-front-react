import React from 'react';
import { Container, Typography } from '@material-ui/core';
import useStyles from '../../assets/styles';

const Footer = () => {

    const classes = useStyles();

    return (
        <div className={classes.footer}>
            <Container>
            <Typography variant="body1" align="center">
                Voluntários de Cascais é uma iniciativa não governamental.
            </Typography>
            </Container>
        </div>
    )
}

export default Footer
