import React from 'react';
import { postServiceAllocation } from '../../services/actions.service';
import { useState } from 'react';
import { Container, TextField, Button, Typography } from '@material-ui/core';
import useStyles from '../../assets/styles';
import ReactSwal from '../../plugins/index';

const FormAllocation = ({id, update, isForm}) => {
    
    const classes = useStyles();

    const [form, setForm] = useState({});

    const handleChange = (e) => {//pega o evento como parametro
        setForm({
            ...form,            
            [e.target.name]:e.target.value
        })
        console.log(form)
    }

    const submitForm = () => {

        const newForm = {
            ...form,
            volunteer_name: form.volunteer_name,
            volunteer_email: form.volunteer_email
        }

        postServiceAllocation(id, newForm)
            .then(()=>{
                ReactSwal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Cadastro realizado.',
                    showConfirmButton: false,
                    timer: 1500
                })
                setForm({});//depois de realizado o cadastro é necessário esvaziar o formulário para nova utilização.
                update(true); //quando seta o update para true a funcao useEffect que está no detalhe percebe que houve alteração e chama de novo o ciclo de vida de render e completa as informações novas no getDetalhes.  
                isForm(false);
            })
            .catch(erro=>console.log('deu ruim...'))
    }
    
        
    return (
        <div className={classes.formContainer}>
            <Container maxWidth="sm">
                <Typography variant="body1">
                    Dados do voluntário:
                </Typography>
                    {/* <FormControl> */}
                <form className={classes.formTextField}>
                <TextField variant="outlined" label="Nome" name="volunteer_name" value={form.volunteer_name || ""} className={classes.outlinedInput} onChange={handleChange} />
                    {/* <OutlinedInput name="volunteer_name" value={form.volunteer_name || ""} className={classes.outlinedInput} onChange={handleChange} type="text" placeholder="Nome"/>
                    <OutlinedInput name="volunteer_email" value={form.volunteer_email || ""} className={classes.outlinedInput} onChange={handleChange} type="email"  placeholder="E-mail"/> */}
                    
                    {/* </FormControl> */}
                    <TextField variant="outlined" label="E-mail" name="volunteer_email" value={form.volunteer_email || ""} className={classes.outlinedInput} onChange={handleChange} />
                 </form>
                 <Button className={classes.button} onClick={submitForm} variant="contained" color="secondary">Cadastrar</Button>
            </Container>
        </div>
    )
}

export default FormAllocation
