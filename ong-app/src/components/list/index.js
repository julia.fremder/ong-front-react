import React from 'react';
import { useState } from 'react';
import { useParams } from 'react-router-dom';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Container, Typography } from '@material-ui/core';
import useStyles from '../../assets/styles';
import styled from 'styled-components';
import PersonIcon from '@material-ui/icons/Person';
import HighlightOffOutlinedIcon from '@material-ui/icons/HighlightOffOutlined';
import { deleteServiceAllocation } from '../../services/actions.service';
import ReactSwal from '../../plugins/index';

const VolunteerList = ({Allocations, update}) => {

    const classes = useStyles();

    const { allocation_id } = useParams();

    const [modal, setModal] = useState({
        modalShow: false,
        data: null
    });

    const deleteAllocation = () => {
        if(modal.data.id){
            deleteServiceAllocation(modal.data.id)
            .then(() => {
                ReactSwal.fire({
                    icon: 'success',
                    title: `Voluntário(a) ${modal?.data?.volunteer_name?.split(" ")[0]} foi descadastrado(a)!`,
                    showConfirmButton: false,
                    showCloseButton: true,
                })
                update(true)
            })
            .catch(erro => alert('a criança não foi excluída devido a' + erro))

    }}

    const toggleModal = (data = null) => {
        // console.log("data",  data)
        setModal({
            modalShow: !modal.modalShow,
            data
            
        })
    }



    return (
        <>
      <Container className={classes.volunteers} maxWidth="sm">
        <Typography variant="h6" gutterBottom={true}>Voluntários:</Typography>
        {Allocations?.map((item, i) => (
          <ul>
            <Li key={i} className="d-flex justify-content-between">
                <div className="d-flex">
                <SPersonIcon fontSize="medium" color="secondary-dark" />
                <Typography align="left">{item.volunteer_name}</Typography>
                </div>
                <Typography align="right">{item.volunteer_email}</Typography>
                <button onClick={()=> toggleModal(item)}><HighlightOffOutlinedIcon size="18" color="primary"/></button>
            </Li>
          </ul>
        ))}
      </Container>
      <Modal className="mt-5" isOpen={modal.modalShow} toggle={toggleModal}>
      <ModalHeader toggle={toggleModal}>Exclusão de voluntário.</ModalHeader>
      <ModalBody>
          Deseja excluir o(a) voluntário(a) <strong>{modal?.data?.volunteer_name?.split(" ")[0]}</strong>?
      </ModalBody>
      <ModalFooter>
          <Button color="success" onClick={() => deleteAllocation()}>SIM</Button>
          <Button color="danger" onClick={toggleModal}>NÃO</Button>
      </ModalFooter>
    </Modal>
    </>
    );
}

export default VolunteerList;

const Li = styled.li`
    list-style: none;
    display: flex;
    align-items: center;
    color: #ba2d65;
    border-bottom: 1px solid #ba2d65;
    height: 40px;
`



const SPersonIcon = styled(PersonIcon)`

    margin-right: 5px;

`