import React from 'react';
import { Link } from 'react-router-dom';
import { Grid, Button } from '@material-ui/core';
import useStyles from '../../assets/styles';

const Navbar = () => {

    const classes = useStyles();

    return (
        <div className={classes.button}>
                <Grid container spacing={2} justify="center">
                  <Grid item>
                    <Button variant="contained" tag={Link} href={`/`} color="primary">
                      Ações
                    </Button>
                    </Grid>
                    <Grid item>
                    <Button variant="outlined" tag={Link} href={`/`} color="primary">
                      Sobre
                    </Button>
                    </Grid>                   
                </Grid>
              </div>
    )
}

export default Navbar

