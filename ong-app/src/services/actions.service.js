import http from '../config/http';


const getServiceAllActions = () => http.get('/actions');

const getServiceAction = (id) => http.get(`/actions/${id}`);

const postServiceAllocation = (id, data) => http.post(`/actions/${id}/allocations/`, data);

const deleteServiceAllocation = (allocation_id) => http.delete(`/actions/${allocation_id}`);

export {

    getServiceAllActions,
    getServiceAction,
    postServiceAllocation,
    deleteServiceAllocation

}