import React from 'react';
import Navbar from '../components/navbar/navbar'
import { useState, useEffect, useCallback } from 'react';
import { getServiceAllActions } from '../services/actions.service';
import { Typography, Grid, Container} from '@material-ui/core';
import CardItem from '../components/cardItem/cardItem';
import Loading from '../components/loading';

import useStyles from '../assets/styles';

const Actions = () => {

    const classes = useStyles();

    const [actions, setActions] = useState([]);
    const [loading, setLoading] = useState(false);

    const getActions = useCallback(()=> {
        setLoading(true)
        getServiceAllActions()
        .then(res => {
            setActions(res.data)
            setLoading(false);
        })
        .catch(err => {

            console.log(' ######## erro' + err);
            setLoading(false);
        })

    }, [])

    useEffect(() => {
        getActions();
    }, [getActions])

    
    const MapActions = (actions) =>
      actions.map((item, i) => (
        <Grid key={i} item xs={12} sm={6} md={4}>
            <CardItem item={{...item}}/>
          
        </Grid>
      ));

    return (
        <>
        <div className={classes.container}>
            <Container maxWidth="sm">
              <Typography variant="h4" align="center" color="primary" gutterBottom>
                Ações Sociais em Cascais
              </Typography>
              <Typography variant="h6" align="center" color="textSecondary" paragraph>
                As ações sociais de Cascais procuram promover a integração da sociedade civil, afim de evitar o isolamento, a depressão e a miséria. Selecione ações para cadastrar voluntários:
              </Typography>
              <Navbar/>
            </Container>
          </div>
          <Container className={classes.cardGrid} maxWidth="md">
          <Grid container spacing={4} justify="center">
            {loading? <Loading/> : MapActions(actions)}
            </Grid>

    </Container>
        </>
    )
}

export default Actions


