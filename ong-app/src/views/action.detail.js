import React from 'react';
import { useParams} from 'react-router';
import Navbar from '../components/navbar/navbar';
import { useCallback, useEffect, useState } from 'react';
import { getServiceAction } from '../services/actions.service';
import { Typography, Container, Button} from '@material-ui/core';
import useStyles from '../assets/styles';
import FormAllocation from '../components/formAllocation/index';
import Loading from '../components/loading'
import VolunteerList from '../components/list';

const ActionDetails = (props) => {

    const { id } = useParams();
    const { history } = props;

    const [loading, setLoading] = useState(false);
    const [actionDetails, setActionDetails] = useState({});
    const [update, setUpdate] = useState(false);
    const [isAllocation, setAllocation] = useState(false);

    const classes = useStyles();

    const getDetails = useCallback(async () => {
            
            try{
                setLoading(true);
                const res = await getServiceAction(id);
                setActionDetails(res.data);
                setLoading(false);

            } catch (err) {
                console.log('##', err)
                history.push('/?error=404')

            }
    }, [id, history]);

    useEffect(() => {
        
        getDetails();
        setUpdate(false)
        
    }, [getDetails, update])

    const AllocateToggleBtn = () => (

            <Container maxWidth="sm" className={classes.allocateBtn}>
                <Typography variant="h6" color="textSecondary">
                {isAllocation ? "Exibir lista de voluntários inscritos" : "Aceda ao formulário de cadastro"}
                </Typography>
                <Button onClick={() => setAllocation(!isAllocation)} color={!isAllocation ? "primary" : "secondary"} size="small">
                    {!isAllocation ? "Formulário" : "Lista"}
                </Button>
            </Container>
)
    
    const displayDetails = (actionDetails) => {

        return (
            <>
            <div  className={classes.container}>
                <Container maxWidth="sm">
                    <Typography variant="h4" align="center" color="primary" gutterBottom>
                        {actionDetails.description}
                    </Typography>
                    <Typography variant="h6" align="center" color="textSecondary" paragraph>
                        {actionDetails.description}
                    </Typography>
                    <Navbar/>
                </Container>
            </div>
            </>
        
        )}

    
    const Allocations = actionDetails.allocations;

    const RenderDetails = (actionDetails) => {

        return (
            <>
            {displayDetails(actionDetails)}
            {AllocateToggleBtn()}
            { 
                isAllocation
                    ? <FormAllocation id={id} update={setUpdate} isForm={setAllocation} />
                    : <VolunteerList Allocations={Allocations} update={setUpdate}/>
                }
            </>
        )
    }
        
    return (
        loading
            ? <Loading/>
            : RenderDetails(actionDetails)            
             // [condicao] ? [true] : [false]       
    )
}

export default ActionDetails;

