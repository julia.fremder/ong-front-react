import React from "react";
import {
    BrowserRouter as BRouter,
    Switch,
    Route,
} from "react-router-dom";

// layout
// import Layout from './components/layout'
import App from "./components/layout";

// views
import Actions from './views/actions';
import ActionDetails from "./views/action.detail";

// import Sobre from './views/sobre';
// import Error404 from './views/errors/404';


const Router = () => {

    return (
        <BRouter>
            <App>
                <Switch>
                    <Route exact path='/' component={Actions} />
                    {/* <Route exact path='/sobre' component={Sobre} /> */}
                    <Route exact path='/actions/:id' component={ActionDetails} />

                    {/* <Route exact to="/error/404" component={Error404} />
                    <Redirect from="*" to="/error/404" />
                    <Route component={Error404} /> */}
                </Switch>
            </App>
        </BRouter >
    )

}


export default Router