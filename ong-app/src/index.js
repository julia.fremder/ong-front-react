import React from 'react';
import ReactDOM from 'react-dom';

import GlobalStyle from './assets/globalStyled';
import Router from './router';
import 'bootstrap/dist/css/bootstrap.min.css';

// import App from './components/layout';

ReactDOM.render(
  <React.Fragment>

    <GlobalStyle/>
    <Router />

  </React.Fragment>,
  
document.getElementById('root'));


